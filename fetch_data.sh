if [ -d "data" ]; then
      echo "Experiments data folder found cached; skipping download."
      return
fi

wget https://linqs-data.soe.ucsc.edu/public/causpsl_data.tar.gz
tar -xvzf causpsl_data.tar.gz
rm *.tar.gz
mv causpsl_data data
