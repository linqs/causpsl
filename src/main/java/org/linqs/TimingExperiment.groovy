package org.linqs;

import org.linqs.psl.config.*;
import java.io.*;
import java.util.*;

public class TimingExperiment{

	private String dataPath = "data/timing_exp/"
	private ConfigBundle cb;
	private String[] settings = ["gauss", "dream"]
	private String[] networkSizes = ['5', '10', '20', '30', '50']
	private String[] setSizes = ["1", "2"]


	public TimingExperiment(String experimentName){
		this.cb = ConfigManager.getManager().getBundle(experimentName);
	}

	public static void main(String[] args){

		TimingExperiment te = new TimingExperiment("timing_exp");
		Map<String,Long> times = new HashMap<String,Long>();

		for(String setting : te.settings){
			for(String ns : te.networkSizes){
				for(String c: te.setSizes){
				
					String expDataPath = te.dataPath + setting + '/' + ns + '/' + c;
					te.cb.setProperty('experiment.data.path', expDataPath);
					te.cb.setProperty('experiment.writePredictions', false);
					te.cb.setProperty('experiment.useACI', true);
					te.cb.setProperty('experiment.useFullACI', true);
					te.cb.setProperty('experiment.usePPI', true);
					te.cb.setProperty('experiment.useJoint', true);
					te.cb.setProperty('experiment.useFullPC', true);

					JointCausalDiscovery jcd = new JointCausalDiscovery(te.cb);
					def timeElapsed = jcd.mainExperiment();	

					times.put(expDataPath, timeElapsed)
					
				}

			}
		}

		for(String setting : times.keySet()){
			System.out.println(setting + ": " + (times[setting]/1000.00).toString());
		}

	}
}
