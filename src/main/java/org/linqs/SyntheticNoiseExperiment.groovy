package org.linqs;

import org.linqs.psl.config.*;
import java.io.*;
import java.util.*;

public class SyntheticNoiseExperiment{

	private ConfigBundle cb;

	private String[] alphas = ['5e-05', '1e-05', '0.0001','0.0005', '0.001', '0.005', '0.01', '0.05', '0.1']

	private Map<String,Integer> bestModelVariantMap = ["dream20Noise":6, "dream30Noise":6]
	private Map<String,Integer> expNumMap = ["dream20Noise":10, "dream30Noise":10]
	private Map<String,String> expNameMap = ["dream30Noise":"data/yeast_combined/dream30Noise/"]


	public SyntheticNoiseExperiment(String experimentName){
		this.cb = ConfigManager.getManager().getBundle(experimentName);
	}

	public static void main(String[] args){

		SyntheticNoiseExperiment sne = new SyntheticNoiseExperiment("synthetic_noise_exp");

		for(String setting : sne.expNameMap.keySet()){

			def dataPath = sne.expNameMap.get(setting);
			def numExperiments = sne.expNumMap.get(setting);
			def variant = sne.bestModelVariantMap.get(setting);
			def noiseRates = 10;

			for(int nr = 0; nr < noiseRates; nr++){
				for(int exp = 0; exp < numExperiments; exp++){
					for(String strictAssocAlpha: sne.alphas){
						for(String statTestAlpha : sne.alphas){

							String expDataPath = dataPath + exp.toString();
							
							sne.cb.setProperty('experiment.data.path', expDataPath);
							sne.cb.setProperty('experiment.strAssocAlpha', 'alpha_' + strictAssocAlpha);
							sne.cb.setProperty('experiment.statTestAlpha', 'alpha_' + statTestAlpha);
							sne.cb.setProperty('experiment.noiseRate', nr);

							boolean useFullPC;
							boolean useFullACI;
							boolean usePPI;
							boolean useJoint;
							boolean useACI;
							def variantName;

							switch(variant){
								case 0:
									useFullPC = true;
									useFullACI = false;
									usePPI = false;
									useJoint = false;
									useACI = false;
									variantName = 'psl-pc';
									break;

								case 1:
									useFullPC = false;
									useFullACI = false;
									usePPI = true;
									useJoint = true;
									useACI = false;
									variantName ='coll+joint';
									break;

								case 2:
									useFullPC = true;
									useFullACI = false;
									usePPI = true;
									useJoint = true;
									useACI = false;
									variantName = 'pc+joint';
									break;

								case 3:
									useFullPC = false;
									useFullACI = false;
									usePPI = true;
									useJoint = true;
									useACI = true;
									variantName = 'coll+joint+anc';
									break;

								case 4:
									useFullPC = false;
									useFullACI = true;
									usePPI = true;
									useJoint = true;
									useACI = true;
									variantName = 'coll+joint+aci';
									break;

								case 5:
									useFullPC = true;
									useFullACI = false;
									usePPI = true;
									useJoint = true;
									useACI = true;
									variantName = 'pc+joint+anc';
									break;

								case 6:
									useFullPC = true;
									useFullACI = true;
									usePPI = true;
									useJoint = true;
									useACI = true;
									variantName = 'pc+joint+aci';
									break;
							}

							String experimentName = 'synthetic_noise_exp/' + setting + '/' + variantName + '/' + nr.toString() + '/' + exp.toString() + '/' + 'strAssocAlpha=' + strictAssocAlpha + ';' + 'statAlpha=' + statTestAlpha;
							sne.cb.setProperty('experiment.name', experimentName);

							sne.cb.setProperty('experiment.useACI', useACI);
							sne.cb.setProperty('experiment.useFullACI', useFullACI);
							sne.cb.setProperty('experiment.usePPI', usePPI);
							sne.cb.setProperty('experiment.useJoint', useJoint);
							sne.cb.setProperty('experiment.useFullPC', useFullPC);

							SyntheticCausal sc = new SyntheticCausal(sne.cb);
							sc.mainExperiment();

							
						}
					}
				}
			}
		}
	}
}
