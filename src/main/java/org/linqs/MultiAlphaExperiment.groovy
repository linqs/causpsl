package org.linqs;

import org.linqs.psl.config.*;
import java.io.*;
import java.util.*;

public class MultiAlphaExperiment{

	private ConfigBundle cb;

	private String[] alphas = ['5e-06', '1e-06', '5e-05', '1e-05', '0.0001','0.0005', '0.001', '0.005', '0.01', '0.05', '0.1']
	private int[] modelVariants = [0, 2, 5, 6];

	private Map<String,Integer> expNumMap = ["dreamMA20":10, "dreamMA30":10, "gaussMA":100]
	private Map<String,String> expNameMap = ["dreamMA20":"data/yeast_combined/dreamMultiAlpha20/", "dreamMA30":"data/yeast_combined/dreamMultiAlpha30/", "gaussMA":"data/gaussMultiAlpha100/"]


	public MultiAlphaExperiment(String experimentName){
		this.cb = ConfigManager.getManager().getBundle(experimentName);
	}

	public static void main(String[] args){

		MultiAlphaExperiment mae = new MultiAlphaExperiment("multi_alpha_exp");

		for(String setting : mae.expNameMap.keySet()){

			def dataPath = mae.expNameMap.get(setting);
			def numExperiments = mae.expNumMap.get(setting);

			for(int exp = 0; exp < numExperiments; exp++){
				for(String strictAssocAlpha: mae.alphas){
					for(String statTestAlpha : mae.alphas){

						String expDataPath = dataPath + exp.toString();
						
						mae.cb.setProperty('experiment.data.path', expDataPath);
						mae.cb.setProperty('experiment.strAssocAlpha', 'alpha_' + strictAssocAlpha);
						mae.cb.setProperty('experiment.statTestAlpha', 'alpha_' + statTestAlpha);

						for(Integer variant : mae.modelVariants){

							boolean useFullPC;
							boolean useFullACI;
							boolean usePPI;
							boolean useJoint;
							boolean useACI;
							def variantName;

							switch(variant){
								case 0:
									useFullPC = true;
									useFullACI = false;
									usePPI = false;
									useJoint = false;
									useACI = false;
									variantName = 'psl-pc';
									break;

								case 1:
									useFullPC = false;
									useFullACI = false;
									usePPI = true;
									useJoint = true;
									useACI = false;
									variantName ='coll+joint';
									break;

								case 2:
									useFullPC = true;
									useFullACI = false;
									usePPI = true;
									useJoint = true;
									useACI = false;
									variantName = 'pc+joint';
									break;

								case 3:
									useFullPC = false;
									useFullACI = false;
									usePPI = true;
									useJoint = true;
									useACI = true;
									variantName = 'coll+joint+anc';
									break;

								case 4:
									useFullPC = false;
									useFullACI = true;
									usePPI = true;
									useJoint = true;
									useACI = true;
									variantName = 'coll+joint+aci';
									break;

								case 5:
									useFullPC = true;
									useFullACI = false;
									usePPI = true;
									useJoint = true;
									useACI = true;
									variantName = 'pc+joint+anc';
									break;

								case 6:
									useFullPC = true;
									useFullACI = true;
									usePPI = false;
									useJoint = true;
									useACI = true;
									variantName = 'pc+joint+aci';
									break;
							}

							String experimentName = 'cv_exp/' + setting + '/' + variantName + '/' + exp.toString() + '/' + 'strAssocAlpha=' + strictAssocAlpha + ';' + 'statAlpha=' + statTestAlpha;
							mae.cb.setProperty('experiment.name', experimentName);

							mae.cb.setProperty('experiment.useACI', useACI);
							mae.cb.setProperty('experiment.useFullACI', useFullACI);
							mae.cb.setProperty('experiment.usePPI', usePPI);
							mae.cb.setProperty('experiment.useJoint', useJoint);
							mae.cb.setProperty('experiment.useFullPC', useFullPC);

							MultiAlphaCausal mac = new MultiAlphaCausal(mae.cb);
							mac.mainExperiment();

						}
					}
				}
			}
		}
	}
}
