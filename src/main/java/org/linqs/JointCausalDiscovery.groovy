package org.linqs;

import java.util.Collections;
import java.util.Iterator;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.linqs.psl.application.inference.MPEInference;
import org.linqs.psl.config.ConfigBundle;
import org.linqs.psl.config.ConfigManager;
import org.linqs.psl.database.Database;
import org.linqs.psl.database.DatabasePopulator;
import org.linqs.psl.database.DataStore;
import org.linqs.psl.database.Partition;
import org.linqs.psl.database.Queries;
import org.linqs.psl.database.ReadOnlyDatabase;
import org.linqs.psl.database.loading.Inserter;
import org.linqs.psl.database.rdbms.driver.H2DatabaseDriver;
import org.linqs.psl.database.rdbms.driver.H2DatabaseDriver.Type;
import org.linqs.psl.database.rdbms.RDBMSDataStore;
import org.linqs.psl.groovy.PSLModel;
import org.linqs.psl.model.atom.GroundAtom;
import org.linqs.psl.model.atom.Atom;
import org.linqs.psl.model.predicate.StandardPredicate;
import org.linqs.psl.model.predicate.Predicate;
import org.linqs.psl.model.term.ConstantType;
import org.linqs.psl.utils.dataloading.InserterUtils;
import org.linqs.psl.utils.evaluation.printing.AtomPrintStream;
import org.linqs.psl.utils.evaluation.printing.DefaultAtomPrintStream;
import org.linqs.psl.utils.evaluation.statistics.SimpleRankingComparator;
import org.linqs.psl.utils.evaluation.statistics.DiscretePredictionComparator;
import org.linqs.psl.utils.evaluation.statistics.DiscretePredictionStatistics;

import com.google.common.collect.Iterables;
import java.io.*;
import java.util.*;

import groovy.time.*;


class JointCausalDiscovery{
    Logger log = LoggerFactory.getLogger(this.class);

    private ExperimentConfig ec;
    private PSLModel model;
    private DataStore data;

    class ExperimentPartitions{

        public Partition testObserved;
        public Partition testTruth;
        public Partition testTargets;

    }

    class ExperimentConfig{
        public ConfigBundle cb;

        Date start;

        public String experimentName;
        public String dbPath;
        public String dataPath;
        public String outputPath;

        public boolean useSquaredPotentials;
        public double initialWeight;
        public boolean writePredictions;
        public boolean appendResults;

        public boolean useFullPC;
        public boolean useFullACI;
        public boolean usePPI;
        public boolean useJoint;
        public boolean useACI;

        public Map<Predicate, String> obsPredicateFileMap;
        public Map<Predicate, String> targetPredicateFileMap;
        public Map<Predicate, String> truthPredicateFileMap;
        public Set<Predicate> binaryPredicates;

        public Set<Predicate> closedPredicates;
        public Set<Predicate> inferredPredicates;

        public ExperimentConfig(ConfigBundle cb){
            this.cb = cb;
            this.start = new Date();

            this.experimentName = cb.getString('experiment.name', 'default');
            this.dbPath = cb.getString('experiment.dbpath', '/tmp/');
            this.dataPath = cb.getString('experiment.data.path', 'data/');
            this.outputPath = cb.getString('experiment.output.outputdir', 'output/jcd_'+this.experimentName+'/');

            this.initialWeight = cb.getDouble('experiment.wl.initweight', 5.0);
            this.useSquaredPotentials = cb.getBoolean('experiment.squared_potentials', true);
            this.writePredictions = cb.getBoolean('experiment.writePredictions', true);
            this.appendResults = cb.getBoolean('experiment.appendResults', false);

            this.useFullPC = cb.getBoolean('experiment.useFullPC', true);
            this.useFullACI = cb.getBoolean('experiment.useFullACI', true);
            this.usePPI = cb.getBoolean('experiment.usePPI', true);
            this.useJoint = cb.getBoolean('experiment.useJoint', true);
            this.useACI = cb.getBoolean('experiment.useACI', true);
        }
    }

    public JointCausalDiscovery(ConfigBundle cb){
        this.ec = new ExperimentConfig(cb);
        this.data = new RDBMSDataStore(new H2DatabaseDriver(Type.Disk, ec.dbPath+'joint_ancestral_causal_inf', true), ec.cb);
        this.model = new PSLModel(this, this.data);
    }


    private void definePredicateInsertionMaps(){

        this.ec.obsPredicateFileMap = [((Predicate)Ancestor):"ancestor.txt",
        ((Predicate)AlwaysAssoc):"always_assoc.txt",
        ((Predicate)Assoc):"assoc.txt",
        ((Predicate)Independent):"indep.txt",
        ((Predicate)InSepSet):"inSepSet.txt",
        ((Predicate)CondIndep):"ci_indep.txt",
        ((Predicate)CondAssoc):"ci_assoc.txt",
        ((Predicate)ValidTarget):"ancestor_targets.txt",
        ((Predicate)HasSize):"has_size.txt",
        ((Predicate)Interacts):"interacts.txt"];

        this.ec.targetPredicateFileMap = [((Predicate)Upstream):"ancestor_targets.txt", ((Predicate)Regulates):"regulates_targets.txt"];
        this.ec.truthPredicateFileMap = [((Predicate)Upstream):"ancestor_truth.txt", ((Predicate)Regulates):"regulates_truth.txt"];
        this.ec.binaryPredicates = [InSepSet, ValidTarget, HasSize, Interacts] as Set;
    }


    private void definePredicates(){


        model.add predicate: "Upstream" , types: [ConstantType.UniqueID, ConstantType.UniqueID];
        model.add predicate: "Regulates", types: [ConstantType.UniqueID, ConstantType.UniqueID];

        model.add predicate: "Ancestor" , types: [ConstantType.UniqueID, ConstantType.UniqueID];
        model.add predicate: "ValidTarget" , types: [ConstantType.UniqueID, ConstantType.UniqueID];

        model.add predicate: "Independent", types: [ConstantType.UniqueID, ConstantType.UniqueID];
        model.add predicate: "Assoc", types: [ConstantType.UniqueID, ConstantType.UniqueID];
        model.add predicate: "AlwaysAssoc", types: [ConstantType.UniqueID, ConstantType.UniqueID];
        model.add predicate: "Interacts", types: [ConstantType.UniqueID, ConstantType.UniqueID];
        
        model.add predicate: "CondIndep", types: [ConstantType.UniqueID, ConstantType.UniqueID, ConstantType.String];
        model.add predicate: "CondAssoc", types: [ConstantType.UniqueID, ConstantType.UniqueID, ConstantType.String];

        model.add predicate: "InSepSet", types: [ConstantType.UniqueID, ConstantType.UniqueID, ConstantType.UniqueID, ConstantType.String];
        model.add predicate: "HasSize", types:[ConstantType.String, ConstantType.String]

         ec.closedPredicates = [Ancestor, ValidTarget, Independent, InSepSet, CondIndep, CondAssoc, HasSize, AlwaysAssoc, Interacts] as Set;
         ec.inferredPredicates = [Upstream, Regulates] as Set;

     }

     private void defineRules(){

        log.info("Defining model rules");
        def initialWeight = ec.initialWeight;

        /*Independence*/

        model.add( rule: (ValidTarget(A, B) & ~AlwaysAssoc(A, B)) >> ~Regulates(A,B), squared : ec.useSquaredPotentials, weight : initialWeight)


        /* Regulates Collider rules */
         model.add( rule : (AlwaysAssoc(C, B) & AlwaysAssoc(A, B)  & ~AlwaysAssoc(A, C) & CondAssoc(A, C, S) & InSepSet(A, C, B, S)) >> Regulates(A, B),
         squared: ec.useSquaredPotentials, weight : initialWeight)

        model.add( rule : (AlwaysAssoc(C, B) & AlwaysAssoc(A, B) & ~AlwaysAssoc(A, C) & CondAssoc(A, C, S) & InSepSet(A, C, B, S)) >> Regulates(C, B),
         squared: ec.useSquaredPotentials, weight : initialWeight)

        /* Mutual exclusivity for regulates */
        model.add( rule: (Regulates(A, B)) >> ~Regulates(B, A), squared : ec.useSquaredPotentials, weight : initialWeight*2)


        if(ec.useFullPC){
            
            model.add( rule : (Regulates(A, B) & Assoc(A, C) & CondIndep(A, C, S) & InSepSet(A, C, B, S) & AlwaysAssoc(B, C)) >> Regulates(B, C),
                squared: ec.useSquaredPotentials, weight : initialWeight)

            model.add( rule : (Regulates(A, B) & Regulates(B, C) & AlwaysAssoc(A, C)) >> Regulates(A, C),
                squared: ec.useSquaredPotentials, weight : initialWeight)       

        }

        if (ec.useJoint){
 
            /*Local Evidence */
            if(ec.dataPath == "data/sachs"){
                model.add( rule: (Ancestor(A, B)) >> Upstream(A, B), squared : ec.useSquaredPotentials, weight : initialWeight)
               
            }
                        
            /* Transitivity of upstream*/
            model.add( rule: (Upstream(A, B) & Upstream(B, C) & (A-C)) >> Upstream(A, C), squared : ec.useSquaredPotentials, weight : initialWeight)

            /* Mutual exclusion */
            model.add( rule: (Upstream(A, B)) >> ~Upstream(B, A), squared : ec.useSquaredPotentials, weight : initialWeight)

            /* Relating regulates and upstream*/
            model.add( rule: (Regulates(A, B)) >> Upstream(A, B), squared : ec.useSquaredPotentials, weight : initialWeight)
            model.add( rule: (~Upstream(A, B)) >> ~Regulates(A, B), squared : ec.useSquaredPotentials, weight : initialWeight)

            model.add( rule: (Upstream(A, B) & AlwaysAssoc(A, B)) >> Regulates(A, B), squared : ec.useSquaredPotentials, weight : initialWeight)

            model.add( rule : (AlwaysAssoc(A, B) & AlwaysAssoc(B, C) & Assoc(A, C) & CondIndep(A, C, S) & InSepSet(A, C, B, S) & Regulates(B, A) & ~Upstream(C, A)) >> Regulates(B, C),
             squared: ec.useSquaredPotentials, weight : initialWeight)

        }

        if (ec.usePPI){

             model.add( rule : (Upstream(A, B) & Interacts(A, B)) >> Regulates(A, B),
                squared: ec.useSquaredPotentials, weight : initialWeight)

        }

        if (ec.useACI){

            model.add( rule: (Independent(A, B)) >> ~Upstream(A, B), squared : ec.useSquaredPotentials, weight : initialWeight)

            model.add( rule : (Independent(Z, W) & CondAssoc(Z, W, S) & InSepSet(Z, W, X, S) & HasSize(S, "1")) >> ~Upstream(X, W),
             squared: ec.useSquaredPotentials, weight : initialWeight)

            model.add( rule : (Independent(Z, W) & CondAssoc(Z, W, S) & InSepSet(Z, W, X, S) & HasSize(S, "1")) >> ~Upstream(X, Z),
             squared: ec.useSquaredPotentials, weight : initialWeight)

            model.add( rule : (Assoc(Z, W) & CondIndep(Z, W, S) & InSepSet(Z, W, X, S) & Upstream(X, W) & Upstream(X, Z) & HasSize(S, "1")) >> ~Upstream(Z, W),
             squared: ec.useSquaredPotentials, weight : initialWeight)

            model.add( rule : (Assoc(Z, W) & CondIndep(Z, W, S) & InSepSet(Z, W, X, S) & Upstream(Z, X) & Upstream(X, W) & HasSize(S, "1")) >> Upstream(Z, W),
             squared: ec.useSquaredPotentials, weight : initialWeight)

            model.add( rule : (Assoc(Z, W) & CondIndep(Z, W, S) & InSepSet(Z, W, X, S) & Upstream(W, X) & Upstream(X, Z) & HasSize(S, "1")) >> Upstream(W, Z),
             squared: ec.useSquaredPotentials, weight : initialWeight)

            // model.add( rule : (Assoc(Z, W) & CondIndep(Z, W, S) & InSepSet(Z, W, X, S) & Upstream(Z, W) & HasSize(S, "1")) >> Upstream(X, W),
            //  squared: ec.useSquaredPotentials, weight : initialWeight)

            // model.add( rule : (Assoc(Z, W) & CondIndep(Z, W, S) & InSepSet(Z, W, X, S) & Upstream(Z, X) & HasSize(S, "1")) >> Upstream(X, W),
            //  squared: ec.useSquaredPotentials, weight : initialWeight)

            // model.add( rule : (Assoc(Z, W) & CondIndep(Z, W, S) & InSepSet(Z, W, X, S) & Upstream(W, Z) & HasSize(S, "1")) >> Upstream(X, Z),
            //  squared: ec.useSquaredPotentials, weight : initialWeight)

            // model.add( rule : (Assoc(Z, W) & CondIndep(Z, W, S) & InSepSet(Z, W, X, S) & Upstream(W, X) & HasSize(S, "1")) >> Upstream(X, Z),
            //  squared: ec.useSquaredPotentials, weight : initialWeight)
        }

        if (ec.useFullACI){

             /*ACI rule 2*/
            model.add(rule : (HasSize(S, "1") & CondIndep(X, Y, S) & InSepSet(X, Y, Z, S) & ~Upstream(X, Z)) >> ~Upstream(X, Y),
             squared: ec.useSquaredPotentials, weight : initialWeight)
        }
    }


    private void loadObservedData(Partition partition){

        for(Predicate pred: ec.obsPredicateFileMap.keySet()){
            String fileName = ec.obsPredicateFileMap[pred];
            def inserter = data.getInserter(pred, partition);

            def fullFilePath = ec.dataPath + '/' + fileName;

            if(ec.binaryPredicates.contains(pred)){
                InserterUtils.loadDelimitedData(inserter, fullFilePath, ',');
            }
            else{
                InserterUtils.loadDelimitedDataTruth(inserter, fullFilePath, ',');
            }
        }
    }


    private void loadInferenceData(ExperimentPartitions ep){

        ep.testObserved = data.getPartition('teObs');
        ep.testTruth = data.getPartition('teTruth');
        ep.testTargets = data.getPartition('teTargets');

        log.info("Setting up partitions for train-test experiment");

        loadObservedData(ep.testObserved);

        loadTargetData(ep.testTargets);
        loadTruthData(ep.testTruth);
        
    }

    private void loadTruthData(Partition partition){

        for(Predicate pred: ec.truthPredicateFileMap.keySet()){
            String fileName = ec.truthPredicateFileMap[pred];
            def inserter = data.getInserter(pred, partition);
            def fullFilePath = ec.dataPath + '/' + fileName;
            InserterUtils.loadDelimitedData(inserter, fullFilePath, ',');
            
        }
    }

    private void loadTargetData(Partition partition){

        for(Predicate pred: ec.targetPredicateFileMap.keySet()){
            String fileName = ec.targetPredicateFileMap[pred];
            def inserter = data.getInserter(pred, partition);
            def fullFilePath = ec.dataPath + '/' + fileName;
            InserterUtils.loadDelimitedData(inserter, fullFilePath, ',');
            
        }
    }

    private void runInference(ExperimentPartitions ep){

        def inferenceEvidencePartition = ep.testObserved;
        def inferenceWritePartition = ep.testTargets;
        def inferenceTruthPartition = ep.testTruth;

        Database inferenceDB = data.getDatabase(inferenceWritePartition, ec.closedPredicates, inferenceEvidencePartition);

        def inferenceApp = new MPEInference(model, inferenceDB, ec.cb);
        inferenceApp.mpeInference();
        inferenceDB.close();
    }

    private void printAtoms(Partition partition, Predicate predicate){

        Database atomsDB = data.getDatabase(partition, [predicate] as Set);

        AtomPrintStream aps = new DefaultAtomPrintStream();
        for (GroundAtom atom : Queries.getAllAtoms(atomsDB, predicate)){
            aps.printAtom(atom);
        }

        aps.close();
        atomsDB.close();
    }

    private void writeOutput(ExperimentPartitions ep){

        def resultsPartition = ep.testTargets;
        def inferredPredicates = ec.inferredPredicates;

        Database resultsDB = data.getDatabase(resultsPartition, inferredPredicates);

        File outputDir = new File(ec.outputPath);

        if(!outputDir.exists()){
            outputDir.mkdirs();
        }

        for(Predicate p : inferredPredicates){
            PrintStream ps = new PrintStream(new File(ec.outputPath + p.getName().toLowerCase()+".txt"));
            AtomPrintStream aps = new DefaultAtomPrintStream(ps);
            Set atomSet = Queries.getAllAtoms(resultsDB, p);
            for(Atom a : atomSet){ aps.printAtom(a); }
            aps.close();
            ps.close();
        }

        resultsDB.close();
    }


    private void writeResults(double precision, double recall){

        File outputDir = new File(ec.outputPath);

        if(!outputDir.exists()){
            outputDir.mkdirs();
        }

        String resultsFile = ec.outputPath + "metrics.txt";

        try{
            def writer = new BufferedWriter(new FileWriter(resultsFile, ec.appendResults));
            StringBuilder output = new StringBuilder();
            output.append(precision + ',' + recall + '\n');
            writer.write(output.toString());
            writer.close();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }


    private void evalResults(ExperimentPartitions ep){
        def inferenceResultsPartition = ep.testTargets;
        def inferenceTruthPartition = ep.testTruth;
        def inferredPredicates = ec.inferredPredicates;

        Database resultsDB = data.getDatabase(inferenceResultsPartition, inferredPredicates);
        Database truthDB = data.getDatabase(inferenceTruthPartition, inferredPredicates);

        DiscretePredictionComparator dpc = new DiscretePredictionComparator(resultsDB);
        dpc.setBaseline(truthDB);
        // dpc.setThreshold(Double.MIN_VALUE);
        dpc.setThreshold(0.3);

        for(Predicate p: inferredPredicates){
            def stats = dpc.compare(p);
            double precision = stats.getPrecision(DiscretePredictionStatistics.BinaryClass.POSITIVE);
            double recall = stats.getRecall(DiscretePredictionStatistics.BinaryClass.POSITIVE);
            double accuracy = stats.getAccuracy();
            log.info(p.toString().toLowerCase() + " Stats: precision: {}; recall: {}; accuracy: {}", precision, recall, accuracy);

            if(!ec.writePredictions){
                this.writeResults(precision, recall);
            }

        }

        resultsDB.close();
        truthDB.close();
    }

    private static ConfigBundle populateConfigBundle(String[] args){
        ConfigBundle cb = ConfigManager.getManager().getBundle("causal-pathways");
        Logger log = LoggerFactory.getLogger(this.class);

        if(args.length > 0){

            def path = args[0].split('/');

            def experimentName;
            if(path.length == 2){
                experimentName = path[1];
            }
            else{
                StringBuilder sb = new StringBuilder();
                for(int i = 1; i < path.length-1; i++){
                    sb.append(path[i] + "_")
                }
                sb.append(path[path.length-1]);
                experimentName = sb.toString();
            }

            cb.setProperty('experiment.data.path', args[0]);
            cb.setProperty('experiment.name', experimentName);

            log.info("Experiment name: " + experimentName);
            log.info("Data path: " + args[0]);
        }

        return cb;
    }

    
    public void mainExperiment(){
        long startTime = System.currentTimeMillis();
        this.definePredicates();
        this.defineRules();

        this.definePredicateInsertionMaps();

        ExperimentPartitions ep = new ExperimentPartitions();

        this.loadInferenceData(ep);

        // this.printAtoms(ep.testObserved, Independent)

        this.runInference(ep);

        long endTime = System.currentTimeMillis();
        long elapsed = endTime - startTime;
        log.info("Time elapsed: " + elapsed/1000.0);

        if (this.ec.writePredictions){
            this.writeOutput(ep);
        }

        this.evalResults(ep);
        
        data.close();
        
    }


    public static void main(String[] args){

        ConfigBundle cb = populateConfigBundle(args);
        JointCausalDiscovery jcd = new JointCausalDiscovery(cb);
        jcd.mainExperiment();

    }
}

